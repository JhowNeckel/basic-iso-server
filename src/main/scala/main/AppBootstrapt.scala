package main

import akka.actor.typed.ActorSystem
import server.Server

object AppBootstrapt {
  def main(args: Array[String]): Unit = {
    ActorSystem(Server("0.0.0.0", 8191), "iso-server")
  }
}
