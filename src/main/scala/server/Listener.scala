package server

import java.util.EventObject

import akka.actor.typed.ActorRef
import org.jpos.iso._
import server.Server.{ Command, Message, ServerUpdate, UnrecognizedSource }

class Listener(server: ActorRef[Command]) {

  val serverEventListener: ISOServerEventListener = new ISOServerEventListener {
    override def handleISOServerEvent(event: EventObject): Unit = server ! ServerUpdate(event)
  }

  val requestListener: ISORequestListener = new ISORequestListener {
    override def process(source: ISOSource, msg: ISOMsg): Boolean = {
      source match {
        case channel: ISOChannel => server ! Message(channel, msg)
        case unrecognized        => server ! UnrecognizedSource(unrecognized)
      }
      true
    }
  }

}