package server

import java.util.EventObject

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ Behaviors, TimerScheduler }
import org.jpos.iso._
import org.jpos.iso.channel.ASCIIChannel
import org.jpos.iso.packager.ISO87APackager

import scala.concurrent.duration.{ FiniteDuration, SECONDS }

object Server {

  sealed trait Command

  case class ServerUpdate(event: EventObject) extends Command
  case class Message(source: ISOChannel, msg: ISOMsg) extends Command
  case class UnrecognizedSource(source: ISOSource) extends Command

  private val packager = new ISO87APackager()

  def apply(address: String, port: Int): Behavior[Command] = Behaviors.setup { context =>
    context.log.info("Starting server at: {}:{}", address, port)

    val channel  = new ASCIIChannel(address, port, packager)
    val server   = new ISOServer(port, channel, null)
    val listener = new Listener(context.self)

    server.addServerEventListener(listener.serverEventListener)
    server.addISORequestListener(listener.requestListener)

    new Thread(server).start()

    Behaviors.withTimers(timers => prepare(timers))
  }

  def prepare(timers: TimerScheduler[Command]): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.receiveMessage {
      case ServerUpdate(event) =>
        updateServer(event, timers)
      case unmatch             =>
        context.log.info("unmatch message {}", unmatch)
        Behaviors.same
    }
  }

  def running(channel: ISOChannel, timers: TimerScheduler[Command]): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.receiveMessage {
      case Message(source, msg) if msg.getMTI == "0200" =>
        val bitmap = new String(msg.pack())

        context.log.info("message received from {} - {}", source.getName, bitmap)
        timers.startSingleTimer(System.currentTimeMillis(), createResponse(source, msg), FiniteDuration(5, SECONDS))

        Behaviors.same
      case Message(source, msg) if msg.getMTI == "0210" =>
        val bitmap = new String(msg.pack())

        //context.log.info("response sent to {} - {}", channel.getName, bitmap)
        //channel.send(msg)
        context.log.info("response sent to {} - {}", source.getName, bitmap)
        source.send(msg)

        Behaviors.same
      case Message(source, msg)                         =>
        context.log.info("Invalid msg: {}", msg)
        source.send(msg)
        Behaviors.same
      case ServerUpdate(event)                          =>
        updateServer(event, timers)
      case unmatch                                      =>
        context.log.info("unmatch message {}", unmatch)
        Behaviors.same
    }
  }

  private def updateServer(event: EventObject, timers: TimerScheduler[Command]): Behavior[Command] =
    Behaviors.setup { context =>
      event match {
        case connection: ISOServerAcceptEvent              =>
          context.log.info("connection received from {}", connection.getISOChannel.getName)
          running(connection.getISOChannel, timers)
        case disconnection: ISOServerClientDisconnectEvent =>
          context.log.info("connection terminated {}", disconnection.getISOChannel.getName)
          Behaviors.same
        case unmatch                                       =>
          context.log.info("unmatch server event or invalid state {}", unmatch)
          Behaviors.same
      }
    }

  private def createResponse(source: ISOChannel, request: ISOMsg): Message = {
    val response = new ISOMsg()
    val mock     = "0210B238000102C0800600000000000000021000000000000010000426110251003104120253042611010020000000001110515350700001110515986144FA TIM confirma sua Recarga no valor de@R$ 10,00 validos por 30 dias.@Para consultar seu saldo disque *222#.@Para saldo promocional disque *767#0160000126600000089009000365340"

    response.setPackager(packager)
    response.unpack(mock.getBytes())

    response.set(4, request.getString(4))
    response.set(7, request.getString(7))
    response.set(11, request.getString(11))
    response.set(32, request.getString(32))
    response.set(41, request.getString(41))
    response.set(42, request.getString(42))

    Message(source, response)
  }
}
