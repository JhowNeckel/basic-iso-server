name := "basic-iso-server"
version := "0.1"
scalaVersion := "2.13.5"

mainClass in Compile := Some("main.AppBootstrapt")

lazy val akka   = "2.6.14"
lazy val jpos   = "2.1.6"
lazy val log4j2 = "2.14.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka"         %% "akka-actor"                 % akka,
  "com.typesafe.akka"         %% "akka-actor-typed"           % akka,
  "org.jpos"                  % "jpos"                        % jpos,
  "org.apache.logging.log4j"  % "log4j-api"                   % log4j2,
  "org.apache.logging.log4j"  % "log4j-core"                  % log4j2,
  "org.apache.logging.log4j"  % "log4j-slf4j-impl"            % log4j2,
  "org.apache.logging.log4j"  % "log4j-layout-template-json"  % log4j2
)
